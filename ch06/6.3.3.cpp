//Arrays and loops: Quiz 2
// Modify the following program so that instead of having maxScore hold the largest score directly, a variable named maxIndex holds the index of the largest score.
//Yang Ma @ PITT PACC
//11/1/2018	


#include <iostream>
 
int main()
{
    int scores[] = { 84, 92, 76, 81, 56 };
    const int numStudents = sizeof(scores) / sizeof(scores[0]);
 
    int maxScore = 0; // keep track of our largest score
    int maxIndex = 0;

    // now look for a larger score
    for (int student = 0; student < numStudents; ++student)
        if (scores[student] > maxScore)
            {maxScore = scores[student]; maxIndex=student;}
 
    std::cout << "The best score was " << maxScore << '\n';
    std::cout << "The best score index is " << maxIndex << '\n';
    return 0;
}