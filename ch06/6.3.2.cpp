//Arrays and loops: Quiz 1
//Yang Ma @ PITT PACC
//10/31/2018	


#include <iostream>
 
 
int main()
{
// Quiz 1: Print the following array to the screen using a loop
    int array[]={ 4, 6, 7, 3, 8, 2, 1, 9, 5 };
    int arraylength =sizeof(array)/sizeof(array[0]);
    for (int i = 0; i < arraylength; i++)
        std::cout << array[i] << "\t";
    std::cout << "\n";

// Quiz 2: Ask the user for a number between 1 and 9. If the user does not enter a number between 1 and 9, repeatedly ask for an integer value until they do. Once they have entered a number between 1 and 9, print the array. Then search the array for the value that the user entered and print the index of that element.

    int number = 0;
    do {
        std::cout << "Enter an integer between 1 and 9: ";
        std::cin >> number;
        // if the user entered something invalid
        if (std::cin.fail())
        {
            std::cin.clear(); // reset any error flags
            std::cin.ignore(32767, '\n'); // ignore any characters in the input buffer
        }
    }while( number<1 || number>9 );
    
    for (int i = 0; i < arraylength; i++)
        std::cout << array[i] << "\t";
    std::cout << "\n";

    for (int i =0; i < arraylength; i++)
    {
        if (number==array[i])
        {
            std::cout << i << "\n";
            break;
        }
    }

    return 0;
}