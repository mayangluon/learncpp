//Arrays and loops
//Yang Ma @ PITT PACC
//10/31/2018	


#include <iostream>
 
 
int main()
{
// Calculate the average score in one class.
    int scores[] = { 84, 92, 76, 81, 56 };
    const int numStudents = sizeof(scores) / sizeof(scores[0]);
    int totalScore = 0;
    
    // use a loop to calculate totalScore
    for (int student = 0; student < numStudents; ++student)
        totalScore += scores[student];
    
    double averageScore = static_cast<double>(totalScore) / numStudents;
    std::cout<< "The average score is " << averageScore <<std::endl;

// Find out the maximum score.
    int maxScore = 0; // keep track of our largest score
    for (int student = 0; student < numStudents; ++student)
        if (scores[student] > maxScore)
            maxScore = scores[student];
 
    std::cout << "The best score was " << maxScore << '\n';

    return 0;
}