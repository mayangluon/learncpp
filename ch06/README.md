## [6.1 — Arrays (Part I)](http://www.learncpp.com/cpp-tutorial/61-arrays-part-i/)
    * In an array variable declaration, we use square brackets ([]) to tell the compiler both that this is an array variable (instead of a normal variable), as well as how many variables to allocate (called the array length).
* Important: Unlike everyday life, where we typically count starting from 1, in C++, arrays always count starting from 0! 
* [Example](6.1.cpp)
* When declaring a fixed array, the length of the array (between the square brackets) must be a compile-time constant. This is because the length of a fixed array must be known at compile time. Here are some different ways to declare fixed arrays.
* In C++, array subscripts must always be an integral type. This includes char, short, int, long, long long, etc… and strangely enough, bool (where false gives an index of 0 and true gives an index of 1). An array subscript can be a literal value, a variable (constant or non-constant), or an expression that evaluates to an integral type.

## [6.2 — Arrays (Part II)](https://www.learncpp.com/cpp-tutorial/62-arrays-part-ii/)
* [Initializing arrays](6.2.1.cpp)
* [Arrays with enum/ enum class](6.2.2.cpp)
* Passing arrays to functions, [Example](6.2.3.cpp): 
    * When a normal variable is passed by value, C++ copies the value of the argument into the function parameter. Because the parameter is a copy, changing the value of the parameter does not change the value of the original argument.
    * However, because copying large arrays can be very expensive, C++ does not copy an array when an array is passed into a function. Instead, the actual array is passed. This has the side effect of allowing functions to directly change the value of array elements!
* Size of arrays, [Example](6.2.4.cpp)
    * The sizeof operator can be used on arrays, and it will return the total size of the array (array length multiplied by element size). Note that due to the way C++ passes arrays to functions, this will not work properly for arrays that have been passed to functions!

## [6.3 — Arrays and loops](https://www.learncpp.com/cpp-tutorial/63-arrays-and-loops/)
* Loops are typically used with arrays to do one of three things [Examples](6.3.cpp):
    * Calculate a value (e.g. average value, total value)
    * Search for a value (e.g. highest value, lowest value).
    * Reorganize the array (e.g. ascending order, descending order)
* Quiz [1&2](6.3.2.cpp), Quiz [3](6.3.3.cpp)

## [6.4 — Sorting an array using selection sort](https://www.learncpp.com/cpp-tutorial/64-sorting-an-array-using-selection-sort/)
* Sorting is generally performed by repeatedly comparing pairs of array elements, and swapping them if they meet some predefined criteria. The order in which these elements are compared differs depending on which sorting algorithm is used. The criteria depends on how the list will be sorted (e.g. in ascending or descending order).
* To swap two elements, we can use the std::swap() function from the C++ standard library, which is defined in the algorithm header. For efficiency reasons, std::swap() was moved to the utility header in C++11. [Example](6.4.1.cpp)
* Selection sort: There are many ways to sort an array. Selection sort is probably the easiest sort to understand, which makes it a good candidate for teaching even though it is one of the slower sorts. Selection sort performs the following steps to sort an array from smallest to largest [Example](6.4.2.cpp):
    * Starting at array index 0, search the entire array to find the smallest value.
    * Swap the smallest value found in the array with the value at index 0
    * Repeat steps 1 & 2 starting from the next index
* [Quiz 1](6.4.3.cpp): Rewrite the selection sort code above to sort in descending order (largest numbers first). Although this may seem complex, it is actually surprisingly simple.
* Another simple sort is called “bubble sort” [Example in Quiz 2](6.4.4.cpp). Bubble sort works by comparing adjacent pairs of elements, and swapping them if the criteria is met, so that elements “bubble” to the end of the array. Although there are quite a few ways to optimize bubble sort, in this quiz we’ll stick with the unoptimized version here because it’s simplest. Unoptimized bubble sort performs the following steps to sort an array from smallest to largest:
    * Compare array element 0 with array element 1. If element 0 is larger, swap it with element 1.
    * Now do the same for elements 1 and 2, and every subsequent pair of elements until you hit the end of the array. At this point, the last element in the array will be sorted.
    * Repeat the first two steps again until the array is sorted.


## [6.5 — Multidimensional Arrays](https://www.learncpp.com/cpp-tutorial/65-multidimensional-arrays/)
* [Example: 2-d array](6.5.cpp)
