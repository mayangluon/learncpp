//Arrays with enum/ enum class 
//Yang Ma @ PITT PACC
//10/31/2018	

#include <iostream>
 
enum StudentNames
{
    KENNY, // 0
    KYLE, // 1
    STAN, // 2
    BUTTERS, // 3
    CARTMAN, // 4
    WENDY, // 5
    MAX_STUDENTS // 6
};

enum class TeamNames
{
    FERRARI, // 0
    REDBULL, // 1
    RENAULT, // 2
    MERCEDES, // 3
    WILLIAMS, // 4
    MAX_TEAMS // 5
};

namespace DriverNames
{
    enum DriverNames
    {
        VETTEL, // 0
        OCON, // 1
        HAMILTON, // 2
        BOTTAS, // 3
        ALONSO, // 4
        RICCIARDO, // 5
        MAX_DRIVERS // 6
    };
 }
int main()
{
    int testScores[MAX_STUDENTS]; // allocate 6 integers
    testScores[STAN] = 76; // still works
    std::cout << testScores[STAN] << "\n";

// Here we cannot directly use raceScores[TeamNames::MAX_TEAMS]
    int raceScores[static_cast<int>(TeamNames::MAX_TEAMS)]; // allocate 5 integers
    raceScores[static_cast<int>(TeamNames::FERRARI)] = 76;
    std::cout << raceScores[static_cast<int>(TeamNames::FERRARI)] << "\n";

// It is better to use standard enum inside a namespace
    int DriverNames[DriverNames::MAX_DRIVERS]; // allocate 6 integers
    DriverNames[DriverNames::VETTEL] = 76; // still works
    std::cout << DriverNames[DriverNames::VETTEL] << "\n";
    return 0;
}