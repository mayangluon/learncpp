//Initializing arrays
//Yang Ma @ PITT PACC
//10/26/2018	

#include <iostream>
 
int main()
{
// Method 1
    int prime[5]; // hold the first 5 prime numbers
    prime[0] = 2;
    prime[1] = 3;
    prime[2] = 5;
    prime[3] = 7;
    prime[4] = 11;
    int n1 = 0;
    while (n1<=4)
    {
        std::cout << n1+1 << "th element of prime is " << prime[n1] << "\n";
        n1++;
    }

// Method 2
    int array[5] = {2,3,5,7,11}; 
    int n2 = 0;
    while (n2<=4)
    {
        std::cout << n2+1 << "th element of array is " << array[n2] << "\n";
        n2++;
    }

    int array2[5] = {2,3,5}; // only initialize first 3 elements
    int n3 = 0;
    while (n3<=4)
    {
        std::cout << n3+1 << "th element of array2 is " << array2[n3] << "\n";
        n3++;
    }

// Omitted length
    int array3[] = { 0, 1, 2, 3, 4 };
    int n4 = 0;
    while (n4<=4)
    {
        std::cout << n4+1 << "th element of array3 is " << array3[n4] << "\n";
        n4++;
    }

    return 0;
}