//Sizeof and arrays
//Yang Ma @ PITT PACC
//10/31/2018	


#include <iostream>
 

void printSize(int array[])
{
    std::cout << sizeof(array) << '\n'; // prints the size of a pointer, not the size of the array!
}
 
int main()
{
    int array[] = { 1, 1, 2, 3, 5, 8, 13, 21 };
    std::cout << "The array has: " << sizeof(array) / sizeof(array[0]) << "elements\n";
    std::cout << sizeof(array) << '\n'; // will print the size of the array
    printSize(array);
    return 0;
}