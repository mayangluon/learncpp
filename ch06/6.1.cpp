//Array
//Yang Ma @ PITT PACC
//10/25/2018	

#include <iostream>
 
int main()
{
    int prime[5]; // hold the first 5 prime numbers
    prime[0] = 2; // The first element has index 0
    prime[1] = 3;
    prime[2] = 5;
    prime[3] = 7;
    prime[4] = 11; // The last element has index 4 (array length-1)
 
    std::cout << "The lowest prime number is: " << prime[0] << "\n";
    std::cout << "The sum of the first 5 primes is: " << prime[0] + prime[1] + prime[2] + prime[3] + prime[4] << "\n";

    // In C++, array subscripts must always be an integral type. This includes char, short, int, long, long long, etc… and strangely enough, bool (where false gives an index of 0 and true gives an index of 1). An array subscript can be a literal value, a variable (constant or non-constant), or an expression that evaluates to an integral type.

    int array[5]; // declare an array of length 5
    
    // using a literal (constant) index:
    array[1] = 7; // ok
    
    // using an enum (constant) index
    enum Animals
    {
        ANIMAL_CAT = 2
    };
    array[ANIMAL_CAT] = 4; // ok
    
    // using a variable (non-constant) index:
    short index = 3;
    array[index] = 7; // ok
    
    // using an expression that evaluates to an integer index:
    array[1+2] = 7; // ok

    return 0;
}