//Quiz
//Yang Ma @ PITT PACC
//10/31/2018	


#include <iostream>
 

double highTemperature[365]={};
namespace animals
{
    enum animals
    {
        CHICKEN,
        DOG,
        CAT,
        ELEPHANT,
        DUCK,
        SNAKE,
        MAX_ANIMALS
    };
}

int main()
{
    int Legs[animals::MAX_ANIMALS] = { 2,4,4,4,2,0 };
    std::cout<<"An elephant has " << Legs[animals::ELEPHANT] << " legs. \n";
    return 0;
}