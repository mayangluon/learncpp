// ++x and x++
//Yang Ma @ PITT PACC
//10/09/2018	
#include <iostream>
 
int main()
{
    
    int x = 5, y = 5;
    std::cout << x << " " << y << '\n';
    // prefix
    std::cout << ++x << " " << --y << '\n'; 
    std::cout << x << " " << y << '\n';
    // postfix
    std::cout << x++ << " " << y-- << '\n'; 
    std::cout << x << " " << y << '\n';
    return 0;
}