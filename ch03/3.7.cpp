// Quiz:
//Write a program that asks the user to input a number between 0 and 255. Print this number as an 8-bit binary number (of the form #### ####). Don’t use any bitwise operators.
//Yang Ma @ PITT PACC
//10/10/2018	
#include <iostream>
 
int getInt();
int calcBit( int, int);

int main()
{
    int a = getInt();
    calcBit(a,128);
    calcBit(a,64);
    calcBit(a,32);
    calcBit(a,16);
    std::cout << " ";
    calcBit(a,8);
    calcBit(a,4);
    calcBit(a,2);
    calcBit(a,1);
    std::cout << "\n";
}

int getInt ()
{
    std::cout << "Enter a number between 0 and 255: " ;
    int x;
    std::cin >> x;
    return x;
}

int calcBit(int x, int pow)
{
    std::cout << (x>=pow) ;
    return x-pow;
}
