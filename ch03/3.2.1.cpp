//Modulus operation, application 
//Yang Ma @ PITT PACC
//10/09/2018	
#include <iostream>
 
int main()
{
    int x = 1;
    while (x<=100)
    {
        std::cout << x << "    ";
        if (x %20 ==0)
            std::cout << "\n";
        x+=1;
    }
  return 0;
}