## [3.2 — Arithmetic operators](https://www.learncpp.com/cpp-tutorial/32-arithmetic-operators/)
* [Use modulus operator "%" to print 20 numbers per line](3.2.1.cpp)
* [Quiz](3.2.2.cpp)
    * Write a program that asks the user to input an integer, and tells the user whether the number is even or odd. Write a function called isEven() that returns true if an integer passed to it is even. Use the modulus operator to test whether the integer parameter is even.

## [3.3 — Increment/decrement operators, and side effects](https://www.learncpp.com/cpp-tutorial/33-incrementdecrement-operators-and-side-effects/)
* [pre and post increment/decrement](3.3.cpp)

## [3.7 — Converting between binary and decimal](https://www.learncpp.com/cpp-tutorial/37-converting-between-binary-and-decimal/)
* [Quiz](3.7.cpp)
    * Write a program that asks the user to input a number between 0 and 255. Print this number as an 8-bit binary number (of the form #### ####). Don’t use any bitwise operators.


