//Quiz
//Write a program that asks the user to input an integer, and tells the user whether the number is even or odd. Write a function called isEven() that returns true if an integer passed to it is even. Use the modulus operator to test whether the integer parameter is even.
//Yang Ma @ PITT PACC
//10/09/2018	
#include <iostream>
 

bool isEven (int);
int getInt ();

int main()
{
    int x = getInt();
    bool a = isEven(x);
    if (a)
    std::cout << "The integer is even." << std::endl;
    else
    std::cout << "The integer is odd." << std::endl;
    return 0;    
}

int getInt ()
{
    std::cout << "Enter an integer: ";
    int x;
    std::cin >> x;
    return x;
}

bool isEven(int x)
{
    return (x%2)==0;
}