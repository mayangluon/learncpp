//Rounding errors
//Yang Ma @ PITT PACC
//10/08/2018	
#include <iostream>
#include <iomanip> 

int main()
{
    double x (0.1);
    std::cout << x << std::endl;
    std::cout << std::setprecision(17);
    std::cout << x << std::endl;

    double y (1);
    std::cout << y << std::endl;
    double z (0.1+0.1+0.1+0.1+0.1+0.1+0.1+0.1+0.1+0.1);
    std::cout << z << std::endl;  
    return 0;
}