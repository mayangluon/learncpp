//NaN and Inf
//Yang Ma @ PITT PACC
//10/08/2018	
#include <iostream>
#include <iomanip> 

int main()
{
    double x (0);
    double y (1);
    double z (-1);
    std::cout << y/x << std::endl;
    std::cout << z/x << std::endl;
    std::cout << x/x << std::endl;
    return 0;
}