//std::boolalpha
//Yang Ma @ PITT PACC
//10/08/2018	
#include <iostream>

int main()
{
    std::cout << true << std::endl;
    std::cout << false << std::endl;
    
    // print bools as true or false
    std::cout << std::boolalpha;

    std::cout << true << std::endl;
    std::cout << false << std::endl;

    // turn it back off
    std::cout << std::noboolalpha;
    
    std::cout << true << std::endl;
    std::cout << false << std::endl;
    
    return 0;
}