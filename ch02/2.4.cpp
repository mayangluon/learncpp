//Overflow
//Yang Ma @ PITT PACC
//10/05/2018	
#include <iostream>
 
int main()
{
    unsigned short x{65535}, y{0};
    std::cout << "x was " << x << std::endl;
    std::cout << "y was " << y << std::endl;
    x = x + 1;
    y = y - 1;
    std::cout << "x is " << x << std::endl;
    std::cout << "y is " << y << std::endl;
    return 0;
}