//Printing chars as integers via type casting
// use static_cast<new_type>(expression) for type casting
//Yang Ma @ PITT PACC
//10/08/2018	
#include <iostream>

bool isPrime (int);

int main()
{
    char ch1 = 97;
    int i = ch1;
    int x = 3;
    std::cout << ch1 << std::endl;
    std::cout << i << std::endl;
    std::cout << static_cast<int>(ch1) << std::endl;
    std::cout << static_cast<char>(x) << std::endl;
    return 0;
}

