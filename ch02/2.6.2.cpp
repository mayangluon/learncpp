//A simple example on if statement
//Yang Ma @ PITT PACC
//10/08/2018	
#include <iostream>

bool isPrime (int);

int main()
{
    std::cout << "Please input a single digit number" << std::endl;
    int a;
    std::cin >> a;
    if ( isPrime(a) )
    std::cout << a << " is a prime number." << std::endl;
    else
    std::cout << a << " is not a prime number." << std::endl;    
    return 0;
}

bool isPrime (int x)
{
    if (x==2)
    return 1;
    else if (x==3)
    return 1;
    else if (x==5)
    return 1;
    else if (x==7)
    return 1;
    else 
    return 0;    
}