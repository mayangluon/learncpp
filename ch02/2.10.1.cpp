//Quiz: Question 3
//Yang Ma @ PITT PACC
//10/08/2018	
#include <iostream>
using namespace std;

double getNumber ();
char getSymbol ();
bool checkSymbol(char);
double calcu(double, double, char);

int main ()
{
    double a = getNumber();
    double b = getNumber();
    char s = getSymbol();
    bool c = checkSymbol (s);
    if (c)
    cout << a << " " << s << " " << b << " is " << calcu(a,b,s) << endl;
    else
    cout << "Invalid symbol." << endl;
    return 0;
}

double getNumber ()
{
    cout << "Enter a double value: ";
    double x;
    cin >> x;
    return x;
}

char getSymbol ()
{
    cout << "Enter one of the following: +, -, *, or /:";
    char x;
    cin >> x;
    return x;
}

bool checkSymbol(char x)
{
    if (x=='+')
    return true;
    else if (x=='-')
    return true;
    else if (x=='*')
    return true;
    else if (x=='/')
    return true;
    else
    return false;
}

double calcu (double a, double b, char c)
{
    if (c=='+')
    return a + b;
    else if (c=='-')
    return a - b;
    else if (c=='*')
    return a * b;
    else if (c=='/')
    return a / b;
}