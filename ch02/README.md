## [2.3 — Variable sizes and the sizeof operator](https://www.learncpp.com/cpp-tutorial/23-variable-sizes-and-the-sizeof-operator/)
* [Check the sizes of the data types](2.3.cpp)

## [2.4 — Integers](https://www.learncpp.com/cpp-tutorial/24-integers/)
* [An example for overflow](2.4.cpp)
    *  For advanced readers, here’s what’s actually happening behind the scenes: the number 65,535 is represented by the bit pattern 1111 1111 1111 1111 in binary. 65,535 is the largest number an unsigned 2 byte (16-bit) integer can hold, as it uses all 16 bits. When we add 1 to the value, the new value should be 65,536. However, the bit pattern of 65,536 is represented in binary as 1 0000 0000 0000 0000, which is 17 bits! Consequently, the highest bit (which is the 1) is lost, and the low 16 bits are all that is left. The bit pattern 0000 0000 0000 0000 corresponds to the number 0, which is our result. 

## [2.5 — Floating point numbers](https://www.learncpp.com/cpp-tutorial/25-floating-point-numbers/)
* [Precision and range](2.5.1.cpp)
    * std::cout has a default precision of 6. It assumes all floating point variables are only significant to 6 digits, and hence it will truncate anything after that. One need to set the precision if it is needed using
    ```
    #include <iomanip>
    std::cout << std::setprecision(16);
    ```
* [Rounding erors](2.5.2.cpp)
    * One of the reasons floating point numbers can be tricky is due to non-obvious differences between binary (how data is stored) and decimal (how we think) numbers. Consider the fraction 1/10. In decimal, this is easily represented as 0.1, and we are used to thinking of 0.1 as an easily representable number. However, in binary, 0.1 is represented by the infinite sequence: 0.00011001100110011… Because of this, when we assign 0.1 to a floating point number, we’ll run into precision problems.

* [Nan and InF](2.5.3.cpp)
    *  Inf represents infinity, while NaN stands for "not a number".

## [2.6 — Boolean values and an introduction to if statements](https://www.learncpp.com/cpp-tutorial/26-boolean-values/)
* [std::boolalpha](2.6.1.cpp)
    *std::cout prints 0 for false, and 1 for true, one can use 
    ```
    std::cout << std::boolalpha
    ```
    print  “true” or “false” instead of 0 or 1, and use
    ```
    std::cout << std::noboolalpha
    ```
    to turn it back off.
* [Using if to tell sigle digit prime number](2.6.2.cpp)

## [2.7 — Chars](https://www.learncpp.com/cpp-tutorial/27-chars/)
* [Printing chars as integers via type casting](2.7.cpp)
    *  To convert between fundamental data types (for example, from a char to an int, or vice versa), we use a type cast called a static cast.
    ```
    static_cast<new_type>(expression)
    ```
## [2.10 — Chapter 2 comprehensive quiz](https://www.learncpp.com/cpp-tutorial/210-comprehensive-quiz/)
* [Question 3](2.10.1.cpp)
* [Question 4, main](2.10.2.cpp), [Question 4, header](2.10.h)