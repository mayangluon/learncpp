//Quiz: Question 4
//Yang Ma @ PITT PACC
//10/08/2018	
#include <iostream>
#include "2.10.h"
using namespace std;
using namespace phyconst;

double getHeight ();
void calHeight (double,double);

int main ()
{
    double h = getHeight ();
    calHeight(h,0);
    calHeight(h,1);
    calHeight(h,2);
    calHeight(h,3);
    calHeight(h,4);
    calHeight(h,5);
    return 0;
}


double getHeight ()
{
    cout << "Enter the height of the tower in meters: ";
    double x;
    cin >> x;
    return x;
}

void calHeight (double h, double t)
{
    double height = h-0.5*g*t*t;
    if (height > 0)
    cout << "At " << t << " seconds, the ball is at height: " << height << " meters" << endl;
    else
    cout << "At " << static_cast<int>(t) << " seconds, the ball is on the ground." << endl;
}

