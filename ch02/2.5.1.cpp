//Precision and range
//std::cout has a default precision of 6
//Yang Ma @ PITT PACC
//10/08/2018	
#include <iostream>
#include <iomanip> // for std::setprecision()

int main()
{
    float x;
    x = 3.1415926f;
    std::cout << x << std::endl;
    double y;
    y= 2.718281828459;
    std::cout << y << std::endl;
    // show 16 digit
    std::cout << std::setprecision(16);
    std::cout << x << std::endl;
    std::cout << y << std::endl;
    double z=3.333333333333333333333333333;
    std::cout << z << std::endl;
    return 0;
}