//Quiz, Question 2
// Implement a game of hi-lo. First, your program should pick a random integer between 1 and 100. The user is given 7 tries to guess the number.

// If the user does not guess the correct number, the program should tell them whether they guessed too high or too low. If the user guesses the right number, the program should tell them they won. If they run out of guesses, the program should tell them they lost, and what the correct number is. At the end of the game, the user should be asked if they want to play again. If the user doesn’t enter ‘y’ or ‘n’, ask them again.
//Yang Ma @ PITT PACC
//10/24/2018	

#include <iostream>

void welcome();
int getInt(int);
int genRand(int,int);
bool again ();
bool play (int);

int main ()
{
    do
    {
        welcome();
        play(genRand(1,100));
    }while (again());
    std::cout << "Thank you for playing.\n";
    return 0;    
}



void welcome()
{
    std::cout << "Let's play a game.  I'm thinking of a number.  You have 7 tries to guess what it is." << "\n";
}

int getInt(int count)
{
    std::cout << "Guess\t#" << count << ":\t";
    int x;
    std::cin >> x;
    return x;
}

int genRand(int min, int max)
{   
    std::srand(5323);
    static const double fraction =1.0/(RAND_MAX+1.0);
    return min + static_cast<int>((max - min + 1) * (std::rand() * fraction));
}

bool again ()
{
    while (true)
    {
        std::cout << "Would you like to play again (y/n)?";
        char yn;
        std::cin>>yn;
        if (yn=='y')
        return true;
        else if (yn=='n')
        return false;
        else
        continue;
    }
}

bool play (int a)
{
    int count = 1;
    bool win = true;
    bool lose = false;
    // while (count <=7)
    // {
    //     int x = getInt(count);
    //     if (x==a)
    //     {
    //         std::cout << "Correct! You win! \n";
    //         return win;
    //     }      
    //     else if (x>a)
    //     {
    //         if (count < 7)
    //         {
    //         std::cout << "Your guess is too high.\n";
    //         count++;
    //         continue;
    //         }
    //         else
    //         {
    //             std::cout << "Sorry, you lose.  The correct number was\t" << a << ".\n";
    //             return lose;
    //         }
    //     }
    //     else
    //     {   
    //         if (count < 7)
    //         {
    //         std::cout << "Your guess is too low.\n";
    //         count++;
    //         continue;
    //         }
    //         else
    //         {
    //             std::cout << "Sorry, you lose.  The correct number was\t" << a << ".\n";
    //             return lose;
    //         }
    //     } 
    // }
    while (count <=7)
    {
        int x = getInt(count);
        if (x==a)
        {
            std::cout << "Correct! You win! \n";
            return win;
        }      
        else if (x>a)
        {
            std::cout << "Your guess is too high.\n";
            count++;
            continue;  
        }
        else
        {   
            std::cout << "Your guess is too low.\n";
            count++;
            continue;
        }
    }
            std::cout << "Sorry, you lose.  The correct number was\t" << a << ".\n";
            return lose;
            
} 
