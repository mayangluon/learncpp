//Quiz
// Invert the nested loops example so it prints the following:
// 5 4 3 2 1
// 4 3 2 1
// 3 2 1
// 2 1
// 1
//Yang Ma @ PITT PACC
//10/17/2018	

#include <iostream>
 
int main()
{
    signed int outer = 5;

    while (outer > 0)
    {
        signed int inner = 0;
    
        while (inner < outer)
        {
            std::cout << outer - inner << " ";
            inner++;
        }

        std::cout << "\n";
        outer--;
    }
 
    return 0;
}


// // Loop between 5 and 1
// int main()
// {
//     int outer = 5;
//     while (outer >= 1)
//     {
//         // loop between inner and 1
//         int inner = outer;
//         while (inner >= 1)
//             std::cout << inner-- << " ";
 
//         // print a newline at the end of each row
//         std::cout << "\n";
//         --outer;
//         }
 
//     return 0;
// }