//Quiz
// Write a program that prints out the letters a through z along with their ASCII codes. Hint: to print characters as integers, you have to use a static_cast.
//Yang Ma @ PITT PACC
//10/17/2018	

#include <iostream>
 
int main()
{
    char letter = 'a';

    while (letter <= 'z')
    {
        std::cout << letter <<" : " << static_cast<int>(letter) << "\n";
        ++letter;
    }
 

    return 0;
}