//Quiz
//Now make the numbers print like this:
//         1
//       2 1
//     3 2 1
//   4 3 2 1
// 5 4 3 2 1
// hint: Figure out how to make it print like this first:

// X X X X 1
// X X X 2 1
// X X 3 2 1
// X 4 3 2 1
// 5 4 3 2 1
//Yang Ma @ PITT PACC
//10/17/2018	

#include <iostream>
 
int main()
{
    signed int outer = 1;

    while (outer <= 5)
    {
        signed int inner1 = 5-outer;
        while (inner1 >0 )
        {
            std::cout << "  ";
            inner1--;
        }
        
        signed int inner2 = 0;
        while (inner2<outer)
        {
            std::cout << " " << outer-inner2;
            inner2++;
        }
        std::cout << "\n";
        outer++;
    }
 
    return 0;
}

// Thanks to Shiva for this solution
#include <iostream>
 
// int main()
// {
// 	// There are 5 rows, we can loop from 1 to 5
// 	int outer = 1;
 
// 	while (outer <= 5)
// 	{
// 		// Row elements appear in descending order, so start from 5 and loop through to 1
// 		int inner = 5;
 
// 		while (inner >= 1)
// 		{
// 			// The first number in any row is the same as the row number
// 			// So number should be printed only if it is <= the row number, space otherwise
// 			if (inner <= outer)
// 				std::cout << inner << " ";
// 			else
// 				std::cout << "  "; // extra spaces purely for formatting purpose
 
// 			--inner;
// 		}
 
// 		// A row has been printed, move to the next row
// 		std::cout << "\n";
 
// 		++outer;
// 	}
 
// 	return 0;
// }