//Quiz
//Write a function called calculate() that takes two integers and a char representing one of the following mathematical operations: +, -, *, /, or % (modulus). Use a switch statement to perform the appropriate mathematical operation on the integers, and return the result. If an invalid operator is passed into the function, the function should print an error. For the division operator, do an integer division.
//Yang Ma @ PITT PACC
//10/16/2018	

#include <iostream>

int getInt();
char getOpt();

int main()
{
    int a = getInt();
    int b = getInt();
    char x = getOpt();
    switch (x)
    {
        case '+':
        std::cout << a << x << b << "=" << a +b << "\n";
        break;
        case '-':
        std::cout << a << x << b << "=" << a -b << "\n";
        break;
        case '*':
        std::cout << a << x << b << "=" << a *b << "\n";
        break;
        case '/':
        std::cout << a << x << b << "=" << a /b << "\n";
        break;
        case '%':
        std::cout << a << x << b << "=" << a %b << "\n";
        break;
        default:
        std::cout << "Invalid operator. \n";
        break;
    }

    return 0;
}

int getInt ()
{
    std::cout << "Enter a integer: ";
    int x;
    std::cin >> x;
    return x;
}

char getOpt()
{
    std::cout << "Enter one of operations: +, -, *, /, or % (modulus)";
    char x;
    std::cin >> x;
    return x; 
}