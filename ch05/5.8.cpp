//Continue in while loop 
//Yang Ma @ PITT PACC
//10/16/2018	

#include <iostream>
 
int main()
{
    int count(0);
    do
    {
        if (count == 5)
            continue; // jump to end of loop body
        std::cout << count << " ";
    
        // The continue statement jumps to here
    } while (++count < 10); // this still executes since it's outside the loop body
    std::cout << "\n";
}