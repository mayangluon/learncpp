//Halt, an example for exit ()
//Yang Ma @ PITT PACC
//10/16/2018	

#include <cstdlib> // needed for exit()
#include <iostream>
 
void cleanup ()
{
    // code here to do any kind of cleanup required
}
 
int main()
{
    std::cout << 1;
    cleanup();
 
    exit(0); // terminate and return 0 to operating system
 
    // The following statements never execute
    std::cout << 2;
    return 0;
}