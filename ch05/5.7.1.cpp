// Quiz
// Write a for loop that prints every even number from 0 to 20.
//Yang Ma @ PITT PACC
//10/17/2018	

#include <iostream>
 
int main()
{
    for (int i=0; i <= 20; i++)
    {
        if (i%2==0)
        std::cout << i << " ";
    }
    std::cout << "\n";
    
    //alternative code, works better
    // for (int count=0; count <= 20; count += 2)
    // std::cout << count << '\n';

    return 0;
}

