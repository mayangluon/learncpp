//Quiz
// Define an enum (or enum class, if using a C++11 capable compiler) named Animal that contains the following animals: pig, chicken, goat, cat, dog, ostrich. Write a function named getAnimalName() that takes an Animal parameter and uses a switch statement to return the name for that animal as a std::string. Write another function named printNumberOfLegs() that uses a switch statement to print the number of legs each animal walks on. Make sure both functions have a default case that prints an error message. Call printNumberOfLegs() from main() with a cat and a chicken. Your output should look like this:
//Yang Ma @ PITT PACC
//10/16/2018	

#include <iostream>
#include <string>

enum class AnimalType
{
    PIG,
    CHICKEN,
    GOAT,
    CAT,
    DOG,
    OSTRICH
};

std::string getAnimalName(AnimalType);
void printNumberOfLegs(AnimalType);

int main()
{
    printNumberOfLegs(AnimalType::CAT);
    printNumberOfLegs(AnimalType::CHICKEN);
    return 0;
}

std::string getAnimalName(AnimalType animalType)
{
    switch (animalType)
    {
        case AnimalType::PIG :
            return "pig";
        case AnimalType::CHICKEN :
            return "chicken";
        case AnimalType::GOAT :
            return "goat";
        case AnimalType::CAT :
            return "cat";
        case AnimalType::DOG :
            return "dog";
        case AnimalType::OSTRICH :
            return "ostrich";  
        default :
            return "unknown" ;
    }
}

void printNumberOfLegs(AnimalType animalType)
{
    switch (animalType)
    {
        case AnimalType::PIG :
            std::cout << "a " << getAnimalName(animalType) << " has " << 4 << " legs. \n";
            break;
        case AnimalType::CHICKEN :
            std::cout << "a " << getAnimalName(animalType) << " has " << 2 << " legs. \n";
            break;
        case AnimalType::GOAT :
            std::cout << "a " << getAnimalName(animalType) << " has " << 4 << " legs. \n";
            break;
        case AnimalType::CAT :
            std::cout << "a " << getAnimalName(animalType) << " has " << 4 << " legs. \n";
            break;
        case AnimalType::DOG :
            std::cout << "a " << getAnimalName(animalType) << " has " << 4 << " legs. \n";
            break;
        case AnimalType::OSTRICH :
            std::cout << "a " << getAnimalName(animalType) << " has " << 2 << " legs. \n";
            break;
        default :
            std::cout << "a " << getAnimalName(animalType) << " has " << " unknown number of legs. \n";
            break;
    }
    
}
