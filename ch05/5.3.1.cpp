//Switch example 1
//Yang Ma @ PITT PACC
//10/16/2018	

#include <iostream>
 
enum Colors
{
    COLOR_BLACK,
    COLOR_WHITE,
    COLOR_RED,
    COLOR_GREEN,
    COLOR_BLUE
};

void printColor(Colors color);
 
int main()
{
    printColor(COLOR_GREEN);
 
    return 0;
}

void printColor(Colors color)
{
    switch (color)
    {
        case COLOR_BLACK:
            std::cout << "Black \n";
            break;
        case COLOR_WHITE:
            std::cout << "White \n";
            break;
        case COLOR_RED:
            std::cout << "Red \n";
            break;
        case COLOR_GREEN:
            std::cout << "Green \n";
            break;
        case COLOR_BLUE:
            std::cout << "Blue \n";
            break;
        default:
            std::cout << "Unknown \n";
            break;
    }
}

// Equivalent function using if statement
// void printColor(Colors color)
// {
//     if (color == COLOR_BLACK)
//         std::cout << "Black";
//     else if (color == COLOR_WHITE)
//         std::cout << "White";
//     else if (color == COLOR_RED)
//         std::cout << "Red";
//     else if (color == COLOR_GREEN)
//         std::cout << "Green";
//     else if (color == COLOR_BLUE)
//         std::cout << "Blue";
//     else
//         std::cout << "Unknown";
// }