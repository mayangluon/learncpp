// Quiz
// Write a function named sumTo() that takes an integer parameter named value, and returns the sum of all the numbers from 1 to value.
// For example, sumTo(5) should return 15, which is 1 + 2 + 3 + 4 + 5.
//Yang Ma @ PITT PACC
//10/17/2018	

#include <iostream>

int getInt();
int sumTo (int);

int main()
{
    int a = getInt();
    std::cout << "The sum is: " << sumTo(a) << "\n";
    return 0;
}

int getInt()
{
    std::cout << "Enter an integer: ";
    int x;
    std::cin >> x;
    return x;
}

int sumTo (int x)
{
    int total=0;
    for (int i=1; i<=x; i++)
    {
        total+=i;
    }
    return total;
}