## [5.1 — Control flow introduction](https://www.learncpp.com/cpp-tutorial/51-control-flow-introduction/)
* [Halt](5.1.cpp)

The most basic control flow statement is the halt, which tells the program to quit running immediately. In C++, a halt can be accomplished through use of the exit() function that is defined in the cstdlib header. The exit function takes an integer parameter that is returned to the operating system as an exit code, much like the return value of main().

## [5.2 — If statements](https://www.learncpp.com/cpp-tutorial/52-if-statements/)

## [5.3 — Switch statements](https://www.learncpp.com/cpp-tutorial/53-switch-statements/)
* [Example 1](5.3.1.cpp)
*  One of the trickiest things about case statements is the way in which execution proceeds when a case is matched. When a case is matched (or the default is executed), execution begins at the first statement following that label and continues until one of the following termination conditions is true:
    * The end of the switch block is reached
    * A return statement occurs
    * A goto statement occurs
    * A break statement occurs
    * Something else interrupts the normal flow of the program (e.g. a call to exit(), an exception occurs, the universe implodes, etc…)
* [Example 2](5.3.2.cpp): what happens if one forgets "break"?
* You can declare (but not initialize) variables inside the switch, both before and after the case labels!
* [Quiz 1](5.3.3.cpp)
* [Quiz 2](5.3.4.cpp)
    ```
    g++ -std=c++11 5.3.4.cpp
    ```
## [5.4 — Goto statements](https://www.learncpp.com/cpp-tutorial/54-goto-statements/)
* [Example](5.4.cpp)
* Avoid use of goto statements unless necessary!

## [5.5 — While statements](https://www.learncpp.com/cpp-tutorial/55-while-statements/)
* [Example](5.5.1.cpp): print numbers
* Rule: Always use signed integers for your loop variables.
* [Quiz 2](5.5.2.cpp), [Quiz 3](5.5.3.cpp), [Quiz 4](5.5.4.cpp)

## [5.6 — Do while statements](https://www.learncpp.com/cpp-tutorial/56-do-while-statements/)
* [Example](5.6.cpp)
* One interesting thing about the above example is that the selection variable must be declared outside of the do block. 
    * If the selection variable were to be declared inside the do block, it would be destroyed when the do block terminates, which happens before the while conditional is executed. But we need the variable to use in the while conditional -- consequently, the selection variable must be declared outside the do block.

## [5.7 — For statements](https://www.learncpp.com/cpp-tutorial/57-for-statements/)
* A for statement is evaluated in 3 parts:
    * The init-statement is evaluated. Typically, the init-statement consists of variable definitions and initialization. This statement is only evaluated once, when the loop is first executed.
    * The condition-expression is evaluated. If this evaluates to false, the loop terminates immediately. If this evaluates to true, the statement is executed.
    * After the statement is executed, the end-expression is evaluated. Typically, this expression is used to increment or decrement the variables declared in the init-statement. After the end-expression has been evaluated, the loop returns to step 2.

* Rule: Test your loops with known inputs that cause it to iterate 0, 1, and 2 times.
* [Quiz 1](5.7.1.cpp), [Quiz 2](5.7.2.cpp)

## [5.8 — Break and continue](https://www.learncpp.com/cpp-tutorial/58-break-and-continue/)
* In the case of a for loop, the end-statement of the for loop still executes after a continue (since this happens after the end of the loop body).
* Be careful when using a continue statement with while or do-while loops. Because these loops typically increment the loop variables in the body of the loop, using continue can cause the loop to become infinite! 
* [Using contine in while loop](5.8.cpp)

## [5.9 — Random number generation](https://www.learncpp.com/cpp-tutorial/59-random-number-generation/)
* A pseudo-random number generator (PRNG) is a program that takes a starting number (called a seed), and performs mathematical operations on it to transform it into some other number that appears to be unrelated to the seed. It then takes that generated number and performs the same mathematical operation on it to transform it into a new number that appears unrelated to the number it was generated from. By continually applying the algorithm to the last generated number, it can generate a series of new numbers that will appear to be random if the algorithm is complex enough.

* Generating random numbers in C++ [Example](5.9.1.cpp)
    * std::srand() sets the initial seed value to a value that is passed in by the caller. srand() should only be called once at the beginning of your program. This is usually done at the top of main().
    * std::rand() generates the next random number in the sequence. That number will be a pseudo-random integer between 0 and RAND_MAX, a constant in cstdlib that is typically set to 32767.

* rand() is perfectly suitable for learning how to program, and for programs in which a high-quality PRNG is not a necessity.
    * One of the main shortcomings of rand() is that RAND_MAX is usually set to 32767 (essentially 15-bits). This means if you want to generate numbers over a larger range (e.g. 32-bit integers), rand() is not suitable. Also, rand() isn’t good if you want to generate random floating point numbers (e.g. between 0.0 and 1.0), which is often useful when doing statistical modelling. Finally, rand() tends to have a relatively short period compared to other algorithms.

* Mersenne Twister: a better method to generate random number [Example](5.9.2.cpp)

## [5.10 — std::cin, extraction, and dealing with invalid text input](https://www.learncpp.com/cpp-tutorial/5-10-stdcin-extraction-and-dealing-with-invalid-text-input/)
* [Example](5.10.cpp)

## [5.x — Chapter 5 comprehensive quiz](https://www.learncpp.com/cpp-tutorial/5-x-chapter-5-comprehensive-quiz/)
* [Quiz 1, header](5.x.1.h), [Quiz 1, main](5.x.1.cpp)
* [Quiz 2a](5.x.2.cpp), [Quiz 2b](5.x.3.cpp)
