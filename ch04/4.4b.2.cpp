//Quiz
//Write a program that asks the user to enter their full name and their age. As output, tell the user how many years they’ve lived for each letter in their name (for simplicity, count spaces as a letter)
//Yang Ma @ PITT PACC
//10/15/2018	
#include <iostream>
#include <string>
 
int main()
{
	std::cout << "Enter your full name: ";
	std::string name;
	std::getline(std::cin, name);
 
    std::cout << "Enter your age: ";
    int age;
    std::cin >> age;

    double x= static_cast<double>(age)/name.length();
    std::cout << "You've lived " << x << " years for each letter in your name." << "\n";

	return 0;
}
