## [4.1a — Local variables, scope, and duration](https://www.learncpp.com/cpp-tutorial/4-1a-local-variables-and-local-scope/)
* [Quiz](4.1.a.cpp)
    * Write a program that asks the user to enter two integers, the second larger than the first. If the user entered a smaller integer for the second integer, use a block and a temporary variable to swap the smaller and larger values. Then print the value of the smaller and larger variables. Add comments to your code indicating where each variable dies.   
    ```
    Enter an integer: 4
    Enter a larger integer: 2
    Swapping the values
    The smaller value is 2
    The larger value is 4
    ```
## [4.2 — Global variables and linkage](https://www.learncpp.com/cpp-tutorial/42-global-variables/)
* [Global variable vs local variable](4.2.1.cpp)
    ```
    the global scope operator (::) can be used to tell the compiler you mean the global version instead of the local version.
    ```
* [Define global variables](4.2.2.cpp), [Call the variables](4.2.3.cpp)
    ```
    g++ 4.2.3.cpp 4.2.2.cpp
    ```
* [Global variables defined in header file](4.2.1.h), [Call the variables](4.2.4.cpp)
* [Global variables declaraed in header file](4.2.2.h), [Define global variables](4.2.5.cpp), [Call the variables](4.2.4.cpp) 
    ```
    g++ 4.2.4.cpp 4.2.5.cpp
    ```
    we recommend defining your constants in the header file. 
    If you find that for some reason those constants are causing trouble, you can move them into a .cpp file. 
    

    * Global variables have global scope, and can be used anywhere in the program. Like functions, you must use a forward declaration (via keyword extern) to use a global variable defined in another file.
    * By default, non-const global variables have external linkage. You can use the static keyword to explicitly make them internal if desired.
    * By default, const global variables have internal linkage. You can use the extern keyword to explicitly make them external if desired.

## [4.3 — Static duration variables](https://www.learncpp.com/cpp-tutorial/43-static-duration-variables/)
* [An example on static duration](4.3.cpp)
* "Static" has two meanings: 
    * when applied to a variable declared outside of a block, it defines a global variable with internal linkage, meaning the variable could only be used in the file in which it was defined.
    * Using the static keyword on local variables changes them from automatic duration to static duration (also called fixed duration). A static duration variable (also called a “static variable”) is one that retains its value even after the scope in which it has been created has been exited! Static duration variables are only created (and initialized) once, and then they are persisted throughout the life of the program.

## [4.3a — Scope, duration, and linkage summary](https://www.learncpp.com/cpp-tutorial/4-3a-scope-duration-and-linkage-summary/)

## [4.3b — Namespaces](https://www.learncpp.com/cpp-tutorial/4-3b-namespaces/)

## [4.3c — Using statements](https://www.learncpp.com/cpp-tutorial/4-3c-using-statements/)

## [4.4b — An introduction to std::string](https://www.learncpp.com/cpp-tutorial/4-4b-an-introduction-to-stdstring/)
* [String input and output](4.4b.1.cpp)
    * To use strings in C++, we first need to #include the <string> header to bring in the declarations for std::string. 
    * Using strings with std::cin may yield some surprises -- Use std::getline() to input text !

        when you enter a numeric value using cin, cin not only captures the numeric value, it also captures the newline. So when we enter 2, cin actually gets the string “2\n”. It then extracts the 2 to variable choice, leaving the newline stuck in the input stream. Then, when std::getline goes to read the name, it sees “\n” is already in the stream, and figures we must have entered an empty string! Definitely not what was intended.
    * A good rule of thumb is that after reading a numeric value with std::cin, remove the newline from the stream. This can be done using the following:
    ```
    std::cin.ignore(32767, "\n"); 
    ```
    It’s the largest signed value guaranteed to fit in a (2-byte) integer on all platforms. Or we can use  the “obscure, complex, and correct” way 
    ```
    #include <limits>
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), "\n"); 
    ```
* [Quiz](4..4b.2.cpp)

## [4.5 — Enumerated types](https://www.learncpp.com/cpp-tutorial/45-enumerated-types/)

 An enumerated type (also called an enumeration) is a data type where every possible value is defined as a symbolic constant (called an enumerator). Enumerations are defined via the enum keyword. 

## [4.7 — Structs](https://www.learncpp.com/cpp-tutorial/47-structs/)
* [Example](4.7.cpp)

## [4.x — Chapter 4 comprehensive quiz](https://www.learncpp.com/cpp-tutorial/4-x-chapter-4-comprehensive-quiz/)
* Quick review

    We covered a lot of material in this chapter. Good job, you’re doing great!

    A block of statements (aka. a compound statement) is treated by the compiler as if it were a single statement. These are placed between curly brackets ({ and }) and used pretty much everywhere.

    Local variables are variables defined within a function. They are created at the point of variable definition, and destroyed when the block they are declared in is exited. They can only be accessed inside the block in which they are declared.

    Global variables are variables defined outside of a function. They are created when the program starts, and are destroyed when it ends. They can be used anywhere in the program. Non-const global variables should generally be avoided because they are evil.

    The static keyword can be used to give a global variable internal linkage, so it can only be used in the file in which it is declared. It can also be used to give a local variable static duration, which means the local variable retains its value, even after it goes out of scope.

    Namespaces are an area in which all names are guaranteed to be unique. Use of namespace is a great way to avoid naming collisions. Avoid use of “using statements” outside of functions.

    Implicit type conversion happens when one type is converted into another type without using a cast. Explicit type conversion happens when one type is converted to another using a cast. In some cases, this is totally safe, and in others, data may be lost. Avoid C-style casts and use static_cast instead.

    std::string offers an easy way to deal with text strings. Strings are always placed between double quotes.

    Enumerated types let us define our own type where all of the possible values are enumerated. These are great for categorizing things. Enum classes work like enums but offer more type safety, and should be used instead of standard enums if your compiler is C++11 capable.

    Typedefs allow us to create an alias for a type’s name. Fixed width integers are implemented using typedefs. Typedefs are useful for giving simple names to complicated types.

    And finally, structs offer us a way to group related variables into a single structure and access them using the member selection operator (.). Object-oriented programming builds heavily on top of these, so if you learn one thing from this chapter, make sure it’s this one.

* [Quiz](4.x.cpp)
    ```
    g++ -std=c++14 4.x.cpp
    ```
