//String input and putput
//Yang Ma @ PITT PACC
//10/15/2018	
#include <iostream>
#include <string>
 
int main()
{
	std::cout << "Pick 1 or 2: ";
	int choice { 0 };
	std::cin >> choice;
 
	std::cin.ignore(32767, '\n'); // ignore up to 32767 characters until a \n is removed
 
	std::cout << "Now enter your name: ";
	std::string name;
	std::getline(std::cin, name);
 
	std::cout << "Hello, " << name << ", you picked " << choice << '\n';
 
    // An example on string appending

    std::string a("45");
    std::string b("11");
 
    std::cout << a + b << "\n"; // a and b will be appended, not added
    a += " volts";
    std::cout << a << "\n";
	return 0;
}
