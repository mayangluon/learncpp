//Define two global variables
//Non-const globals have external linkage by default
//Yang Ma @ PITT PACC
//10/11/2018	

int g_x; // external linkage by default
extern const int g_y(2); // external linkage by default, so this extern is redundant and ignored
