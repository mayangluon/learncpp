//Struct
//Yang Ma @ PITT PACC
//10/16/2018	
#include <iostream>
#include <string>

struct Books
{
    std::string title;
    std::string author;
    std::string subject;
    int page;    
};

void printBook (Books book)
{
    std::cout << "Book title: " << book.title << "\n";
    std::cout << "Book author: " << book.author << "\n";
    std::cout << "Book subject: " << book.subject << "\n";
    std::cout << "Book page: " << book.page << "\n";
}

int main ()
{
    Books book1;
    book1.title = "An Introduction to Quantum Field Theory";
    book1.author = "Peskin";
    book1.subject = "Physics";
    book1.page = 800;
    printBook(book1);

    Books book2 ={"Particle Physics", "Martin", "Physics", 500 };
    printBook(book2);

    if (book1.page==book2.page)
    std::cout << "They have same numbers of pages! \n";
    std::cout << "They have different numbers of pages! \n";
 
    return 0;
}
