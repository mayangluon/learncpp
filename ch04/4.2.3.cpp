//Call two global variables from another file
//Yang Ma @ PITT PACC
//10/11/2018	
#include<iostream>

// forward declaration for g_x, g_x can now be used beyond this point in this file
extern int g_x;

int main ()
{
// forward declaration for g_y, g_y can be used beyond this point in main() only
    extern int g_y;

    g_x = 5;
    std::cout << g_x << "\n" << g_y << "\n";
    return 0;
}



