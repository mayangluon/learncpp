//Quiz
// In designing a game, we decide we want to have monsters, because everyone likes fighting monsters. Declare a struct that represents your monster. The monster should have a type that can be one of the following: an ogre, a dragon, an orc, a giant spider, or a slime. If you’re using C++11, use an enum class for this. If you’re using an older compiler, use an enumeration for this.

// Each individual monster should also have a name (use a std::string), as well as an amount of health that represents how much damage they can take before they die. Write a function named printMonster() that prints out all of the struct’s members. Instantiate an ogre and a slime, initialize them using an initializer list, and pass them to printMonster().
//Yang Ma @ PITT PACC
//10/16/2018	
#include <iostream>
#include <string>

enum class MonsterType
{
    ORGE,
    DRAGON,
    ORC,
    GIANT_SPIDER,
    SLIME
};

struct Monster
{
    MonsterType type;
    std::string name;
    int health;    
};

std::string getMonsterType (Monster monster);
void printMonster (Monster monster);

int main ()
{
    Monster torg = {MonsterType::ORGE,"Torg",145};
    Monster blurp {MonsterType::SLIME,"Blurp", 23};
    printMonster (torg);
    printMonster (blurp);

    return 0;
}

std::string getMonsterType(Monster monster)
{
    if (monster.type==MonsterType::ORGE)
    return "orge";
    else if (monster.type==MonsterType::DRAGON)
    return "dragon";
    else if (monster.type==MonsterType::ORC)
    return "orc";
    else if (monster.type==MonsterType::GIANT_SPIDER)
    return "giant spider";
    else if (monster.type==MonsterType::SLIME)
    return "slime"; 
    else 
    return "unknown";   
}

void printMonster (Monster monster)
{
    std::cout << "The " << getMonsterType(monster) << " is named " << monster.name 
              << " and has " << monster.health << " health \n";
}


