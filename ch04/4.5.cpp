//Quiz
//Yang Ma @ PITT PACC
//10/15/2018	
#include <iostream>

// Define an enumerated type to choose between the following monster races: orcs, goblins, trolls, ogres, and skeletons.
enum Races
{
    RACE_ORCS,
    RACE_GOBLINS,
    RACE_TROLLS,
    RACE_OGRES,
    RACE_SKELETONS
};

Races x = RACE_TROLLS;


int main()
{
    std::cout << x << "\n";
	return 0;
}
