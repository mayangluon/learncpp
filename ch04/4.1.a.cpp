// Quiz:
//Write a program that asks the user to enter two integers, the second larger than the first. If the user entered a smaller integer for the second integer, use a block and a temporary variable to swap the smaller and larger values. Then print the value of the smaller and larger variables. Add comments to your code indicating where each variable dies.
//Yang Ma @ PITT PACC
//10/11/2018	
#include <iostream>
 
int getInt();


int main()
{   
    int x,y;
    {
        std::cout << "Enter an integer: " ;
        std::cin >> x;
    }

    {
        std::cout << "Enter a larger integer: " ;
        std::cin >> y;
    }

    if (x<y)
    std::cout << "The smaller value is " << x << "\n"
              << "The larger value is " << y << "\n";
    else
    {
        std::cout << "Swapping the values" << "\n";
        int a = y;
        int b = x;
        std::cout << "The smaller value is " << a << "\n"
                  << "The larger value is " << b << "\n"; 
    }
    return 0;
}

