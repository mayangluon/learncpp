//Define variables in header files
//Yang Ma @ PITT PACC
//10/11/2018	
#ifndef CONSTANTS_H
#define CONSTANTS_H

namespace constants
{
    const double pi (3.14159);
    const double g  (9.8);
}
#endif

