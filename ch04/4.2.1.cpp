// Global varibale vs local variable
//Yang Ma @ PITT PACC
//10/11/2018	
#include <iostream>

 // define global variable
int value(5);
 
int main()
{
    // Define a local variable "value", which hides the global variable value
    int value = 7; 
    value++; // increments local value, not global value
    ::value--; // decrements global value, not local value
 
    std::cout << "global value: " << ::value << "\n";
    std::cout << "local value: " << value << "\n";
    return 0;
} 
