//Static duration
//Yang Ma @ PITT PACC
//10/11/2018	
#include <iostream>
 
void incrementAndprint()
{
    int a = 1;
    static int b = 1;
    ++ a;
    ++ b;
    std::cout << "a=" << a << ", b=" << b << "\n";
}

int main()
{   
    incrementAndprint ();
    incrementAndprint ();
    incrementAndprint ();
    incrementAndprint ();
    return 0;
}

