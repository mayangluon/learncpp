## Learn C++
Here is my note on reviewing C++ with https://www.learncpp.com/
### Compilers recommended
 * Linux: g++ 5.0+     
```
g++ -std=c++14 some_ex.cpp -o some_ex
```

### Contents
- [Chapter 0: HELLO WORLD!](ch0/helloworld.cpp)
- [Chapter 1: C++ Basics](./ch01/README.md)
- [Chapter 2: Variables and Fundamental Data Types](./ch02/README.md)
- [Chapter 3: Operators](./ch03/README.md)
- [Chapter 4: Variable Scope and More Types](./ch04/README.md)
- [Chapter 5: Control Flow](./ch05/README.md)
- [Chapter 6: Arrays, Strings, Pointers, and References](./ch06/README.md)
