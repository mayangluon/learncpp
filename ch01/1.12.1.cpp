//Quiz: Question 1
//Yang Ma @ PITT PACC
//9/28/2018

#include <iostream>

int readNumber ();
int writeNumber (int);

int main ()
{
    int x= readNumber();
    int y= readNumber();
    writeNumber(x+y);
    return 0;
}

int readNumber ()
{
    std::cout << "Please enter a single integer" << std::endl;
    int x;
    std::cin >> x;
    return x;
}

int writeNumber (int x)
{
    std::cout << " The sum of the two integers is " << x << std::endl;
    return 0;
}