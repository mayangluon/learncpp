//Quiz: Question 2
//Yang Ma @ PITT PACC
//9/28/2018

#include <iostream>
int readNumber ()
{
    std::cout << "Please enter a single integer" << std::endl;
    int x;
    std::cin >> x;
    return x;
}

int writeNumber (int x)
{
    std::cout << " The sum of the two integers is " << x << std::endl;
    return 0;
}