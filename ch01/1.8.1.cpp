//Functions in different files
//Yang Ma @ PITT PACC
//9/27/2018

#include <iostream>
using namespace std;

// needed so main.cpp knows that add() is a function declared elsewhere
int add (int, int);

int main ()
{
    cout << "3+4=" << add(3,4) << endl;
    return 0;
}