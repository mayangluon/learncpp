//cin and cout
//Yang Ma @ PITT PACC
//9/26/2018

#include <iostream>
using namespace std;

int main()
{
    double a;
    cout << "Hi!" << endl;
    cout << "This is a test of cout, please enter a number:" << endl;
    cin >> a;
    cout << "The number you entered is " << a << endl; 
    return 0;
}
