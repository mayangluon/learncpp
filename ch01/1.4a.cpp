//Function parameter and argument
//Yang Ma @ PITT PACC
//9/26/2018

#include <iostream>
using namespace std;

//No parameter
void doPrint ()
{
    cout << "doPrint()" << endl;
}

//One integer parameter x
void printValue (int x)
{
    cout << x << endl;
}

//Two integer parameters x, y
int add (int x, int y)
{
    return x+y;
}

int main ()
{
    doPrint();
    printValue(2);
    cout << add (3,4) << endl;
    return 0;
}