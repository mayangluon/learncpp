//Header files
//Yang Ma @ PITT PACC
//9/27/2018

//Header gurd, ADD_H can be any unique name.  By convention, we use the name of the header file.
#ifndef ADD_H
#define ADD_H

int add (int x, int y);

#endif 