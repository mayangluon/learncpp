## [1.3 — A first look at variables, initialization, and assignment](https://www.learncpp.com/cpp-tutorial/13-a-first-look-at-variables-initialization-and-assignment/)

* [Uninitialized variables](1.3.cpp) : In this case, the computer will assign some unused memory to x.


## [1.3a — A first look at cout, cin, and endl](https://www.learncpp.com/cpp-tutorial/1-3a-a-first-look-at-cout-cin-endl/)

* [cin and cout](1.3a.cpp)

## [1.4 — A first look at functions and return values](https://www.learncpp.com/cpp-tutorial/14-a-first-look-at-functions/)
* [Function 1](1.4.cpp)
    * The same function can be called multiple times.
    * Note that main() isn’t the only function that can call other functions. Any function can call another function!
    * Functions can not be defined inside other functions (called nesting) in C++.

## [1.4a — A first look at function parameters and arguments](https://www.learncpp.com/cpp-tutorial/1-4a-a-first-look-at-function-parameters/)
* [Function 2, parameter and argument](1.4a.cpp)
    * Each function’s parameters are only valid within that function. So even though printValue() and add() both have a parameter named x, these parameters are considered separate and do not conflict.
    * An argument is a value that is passed from the caller to the function when a function call is made.
    ```
    When a function is called, all of the parameters of the function are created as variables, and the value
    of each of the arguments is copied into the matching parameter. This process is called pass by value.
    ```
## [1.7 — Forward declarations and definitions](https://www.learncpp.com/cpp-tutorial/17-forward-declarations/)
* [Function 3, declaration](1.7.cpp)
    * A forward declaration allows us to tell the compiler about the existence of an identifier before actually defining the identifier.
    *  If a forward declaration is made, but the function is never called, the program will compile and run fine. However, if a forward declaration is made, the function is called, but the program never defines the function, the program will compile okay, but the linker will complain that it can’t resolve the function call.

## [1.8 — Programs with multiple files](https://www.learncpp.com/cpp-tutorial/18-programs-with-multiple-files/)
* [Function 4, main](1.8.1.cpp)
* [Function 4, add](1.8.2.cpp)

    *  When you compile your program, you’ll need to include all of the relevant code files on the compile line. For example: 
    ```
    g++ 1.8.1.cpp 1.8.2.cpp -o main
    ```
    * Do not #include “add.cpp” from main.cpp. This will cause the compiler to insert the contents of add.cpp directly into main.cpp instead of treating them as separate files. While it may compile and run for this simple example, you will encounter problems down the road using this method. We have such an ecemple in [1.8.3.cpp](1.8.3.cpp).

## [1.9 — Header files](https://www.learncpp.com/cpp-tutorial/19-header-files/)
* [Function 5, header](1.9.h)
* [Function 5, main](1.9.1.cpp)
* [Function 5, add](1.9.2.cpp)
    ```
    g++ 1.9.1.cpp 1.9.2.cpp
    ```
    * Angled brackets vs quotes
        * Angled brackets are used to tell the compiler that we are including a header file that was included with the compiler, so it should look for that header file in the system directories. 
        * The double-quotes tell the compiler that this is a header file we are supplying, so it should look for that header file in the current directory containing our source code first.

        ```
        Rule: Use angled brackets to include header files that come with the compiler. 
        Use double quotes to include any other header files.
        ```
* Header file best practices
    * Always include header guards.
    * Do not define variables in header files unless they are constants. Header files should generally only be used for declarations.
    * Do not define functions in header files.
    * Each header file should have a specific job, and be as independent as possible. For example, you might put all your declarations related to functionality A in A.h and all your declarations related to functionality B in B.h. That way if you only care about A later, you can just include A.h and not get any of the stuff related to B.
    * Give your header files the same name as the source files they’re associated with (e.g. grades.h goes with grades.cpp).
    * Try to minimize the number of other header files you #include in your header files. Only #include what is necessary.
    * Do not #include .cpp files.

##[1.12 — Chapter 1 comprehensive quiz](https://www.learncpp.com/cpp-tutorial/112-comprehensive-quiz/)
* [Quiz: Function](1.12.1.cpp)
* [main.cpp](1.12.2.cpp), [io.cpp](1.12.3.cpp)
    ```
    g++ 1.12.2.cpp 1.12.3.cpp
    ```
* [main.cpp](1.12.4.cpp), [header](1.12.h), [io.cpp](1.12.3.cpp)
    ```
    g++ 1.12.4.cpp 1.12.3.cpp
    ```
