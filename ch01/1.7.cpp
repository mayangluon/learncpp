//Function declaration
//Yang Ma @ PITT PACC
//9/26/2018

#include <iostream>
using namespace std;

//Forward declaration
int add (int, int);

int main ()
{
    cout << "1+1=" << add(1,1) << endl;
    return 0;
}

int add (int x, int y)
{
    return x + y;
}