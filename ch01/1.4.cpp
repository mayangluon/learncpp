//Reusing functions: The same function can be called multiple times, which is useful if you need to do something more than once.
//Any function can call another function
//Yang Ma @ PITT PACC
//9/26/2018

#include <iostream>
using namespace std;

double getValueFromeUser()
{
    double a;
    cout << "Enter a number" << endl;
    cin >> a;
    return a;
}

double sum(double a, double b)
{
    return a+b;
}

void add(double a, double b)
{
    cout << "x plus y is " << sum(a,b) << endl;
}

int main ()
{
    double x = getValueFromeUser();
    double y = getValueFromeUser();
    cout << x << "+" << y << "=" << sum(x,y) << endl;
    add(x,y);
}